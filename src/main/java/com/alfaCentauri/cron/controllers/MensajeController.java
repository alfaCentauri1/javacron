package com.alfaCentauri.cron.controllers;

import com.alfaCentauri.cron.services.Mensaje;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MensajeController {
    @Autowired
    Mensaje mensaje;

    @RequestMapping(value = "/saludo")
    public String saludar(){
        System.out.println("Hola mundo desde Spring Boot.");
        return mensaje.imprimirMensaje();
    }
}
