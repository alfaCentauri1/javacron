package com.alfaCentauri.cron.services;

import org.springframework.stereotype.Service;

@Service
public class Mensaje {

    public String imprimirMensaje(){
        return "Hola desde Spring Boot";
    }
}
