package com.alfaCentauri.cron;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class JavaCronApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaCronApplication.class, args);
	}

}
